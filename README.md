# Elm : Getto.Http.Header

http header utilities


###### Table of Contents

- [Requirements](#requirements)
- [Usage](#usage)
- [Contributes](#contributes)
- [License](#license)

## Requirements

- elm : 0.19


## Usage

- elm packages doc : [getto-systems/elm-http-header](https://package.elm-lang.org/packages/getto-systems/elm-http-header/latest/)

### Install

```
$ elm install getto-systems/elm-http-header
```


## Contributes

### Test

fix codes, and pass tests

```
$ npm test
```


### Update

run `bin/bump.sh`, and create release-request

```
$ ./bin/bump.sh
$ git add elm.json
$ git release-request exact "RELEASE MESSAGE"
```


## License

getto-transition is licensed under the [MIT](LICENSE) license.

Copyright &copy; since 2018 shun@getto.systems
